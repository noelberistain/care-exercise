import React from "react"
import { DropDown } from "./DropDown"

export const Item = ({
  id,
  last,
  number,
  type,
  setType,
  handleChange,
  handleDelete,
  handleAdd,
}) => {
  return (
    <li style={{ listStyle: "none" }}>
      <DropDown type={type} id={id} setType={setType} />
      <input type='text' value={number} onChange={handleChange} />
      <button onClick={() => handleDelete(id)}>x</button>
      {/* Add button only shows up if 'last' prop is true */}
      {last && <button onClick={handleAdd}>+</button>}
    </li>
  )
}
