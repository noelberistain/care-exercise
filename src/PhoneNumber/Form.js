import React, { useState } from "react"
import { List } from "./List"
import uuid from "react-uuid"
import { data } from "../utils.js/mockData"

export const Form = () => {
  const [state, setState] = useState(data)

  const [input, setInput] = useState("")

  /**Handler for creating a new entry and attached it to the state */
  const handleAdd = (event) => {
    event.preventDefault()
    const aux = [...state]
    if (aux.length > 0) aux[aux.length - 1].last = false

    const newItem = {
      id: uuid(),
      phone: "",
      type: "Home",
      status: "new",
      last: true,
    }
    setState([...aux, newItem])
  }

  /**Handler for save the entry with new values */
  const saveEntry = () => {
    const aux = state.map((item) => {
      if (item.status === "new") {
        item.phone = input
        item.status = "saved"
      }
      return item
    })
    setState(aux)
    setInput("")
  }

  /**Handler for updating the selected type on dropdown */
  const setType = (id, typeSelected) => {
    const aux = state.map((entry) => {
      if (entry.id === id) {
        entry.type = typeSelected
      }
      return entry
    })
    setState(aux)
  }

  /**Handler for deleting an entry using the 'id' prop*/
  const handleDelete = (id) => {
    const aux = state.filter((data) => data.id !== id)
    if (aux.length > 0) {
      aux[aux.length - 1].last = true
    }
    setState(aux)
  }

  /**Handler to manipulate the change on the new entries */
  const handleChange = (e) => {
    setInput(e.target.value)
  }

  /**After having at least 10 digits, 'save' the entry
   * By now entry is going to be 'saved' automatically when
   * the user reaches the 10 digits
   */
  input.length >= 10 && saveEntry()

  return (
    <div>
      <List
        input={input}
        phoneNumbers={state}
        setType={setType}
        handleAdd={handleAdd}
        handleDelete={handleDelete}
        handleChange={handleChange}
      />
    </div>
  )
}
