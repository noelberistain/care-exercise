import uuid from "react-uuid"
export const data = [
  {
    id: uuid(),
    phone: "1233214554",
    type: "Home",
    status: "saved",
    last: false,
  },
  {
    id: uuid(),
    phone: "6652136458",
    type: "Mobile",
    status: "saved",
    last: false,
  },
  {
    id: uuid(),
    phone: "6456536458",
    type: "Home",
    status: "saved",
    last: false,
  },
  {
    id: uuid(),
    phone: "6652225458",
    type: "Other",
    status: "saved",
    last: true,
  },
]
