import React, { useState } from "react"

export const DropDown = ({ type, id, setType }) => {
  const [select, setSelect] = useState(type)

  const handleSelect = (e) => {
    setSelect(e.target.value)
    setType(id, e.target.value)
  }
  return (
    <select value={select} onChange={handleSelect}>
      <option value='Home'>Home</option>
      <option value='Mobile'>Mobile</option>
      <option value='Other'>Other</option>
    </select>
  )
}
