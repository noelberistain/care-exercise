import React from "react"
import { Item } from "./Item"

export const List = ({
  input,
  phoneNumbers,
  setType,
  handleAdd,
  handleChange,
  handleDelete,
}) => {
  const list =
    phoneNumbers.length > 0 ? (
      phoneNumbers.map((phoneNumber) => {
        if (phoneNumber.phone) {
          return (
            /**If we have a 'phone' render the Item with proper values and handlers */
            <Item
              key={phoneNumber.id}
              id={phoneNumber.id}
              last={phoneNumber.last}
              number={phoneNumber.phone}
              type={phoneNumber.type}
              setType={setType}
              handleChange={handleChange}
              handleAdd={handleAdd}
              handleDelete={handleDelete}
            />
          )
        } else {
          return (
            /**If there are no 'phone' we render a new Item ready to type in, with its handlers*/
            <Item
              key={phoneNumber.id}
              id={phoneNumber.id}
              last={phoneNumber.last}
              number={input}
              setType={setType}
              handleChange={handleChange}
              handleAdd={handleAdd}
              handleDelete={handleDelete}
            />
          )
        }
      })
    ) : (
      /**If list is empty display info and give the button to start*/
      <>
        "Nothing here"
        <button onClick={handleAdd}>Add a new Entrie</button>
      </>
    )

  return <ul>{list}</ul>
}
